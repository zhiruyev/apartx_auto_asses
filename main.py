import pandas
from sklearn.linear_model import LinearRegression
import pickle

url = "./Housing.csv"
names = ['price', 'area']
dataframe = pandas.read_csv(url, usecols=names)
array = dataframe.values
X = array[:, 1]
X = [x / 100000000 for x in X]
X = [[x] for x in X]
Y = array[:, 0]
Y = [y / 1000000000 for y in Y]
Y = [[y] for y in Y]
test_size = 0.33
seed = 7
# Fit the model on training set
model = LinearRegression()
model.fit(X, Y)
# save the model to disk
filename = 'finalized_model.sav'
pickle.dump(model, open(filename, 'wb'))

# some time later...

# load the model from disk
loaded_model = pickle.load(open(filename, 'rb'))
result = loaded_model.predict([[10]])
print(result)